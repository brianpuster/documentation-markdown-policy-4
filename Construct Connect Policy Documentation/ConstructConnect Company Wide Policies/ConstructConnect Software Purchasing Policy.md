title: Software Purchasing Policy
author: Dave Storer - Controller
published: 2019-08-08

#### Policy level: 
- Very Important 

#### Author: 
- Dave Storer, Controller 

#### Approver(s):
- Bob Ven, CTO
- Buck Brody EVP Finance 

#### What location(s) and/or team(s) does the policy apply to?
- All locations, all teams 

#### Effective date: 
- 2019-08-15 

---

#### Policy text: 
It is the responsibility of the requestor to complete the proper forms and obtain the appropriate approvals.

All purchases made under the Reqlogic Item ID categories: software licenses, IT maintenance and support and custom analytics direct costs require approval by VP of IT Operations.


Additionally, for new purchases greater than $5,000 (based on Annual Contract Value) OR that 

(i) Directly interacts with one of our customer-facing products regardless of price

(ii) Interface or store Personally Identifiable Information (PII).  This applies to both customer-facing and internal systems (such as backoffice systems)

(iii) Interface or store other red data (including customer/prospect lists, trade secrets, or company confidential material). This applies to both customer-facing and internal systems (such as backoffice systems)


A memo is required that outlines:

- The business issue
- How this solution solves that issue
- Listing of competitors contacted
- Reason for selection of this vendor
- List of existing systems impacted
- Who will serve as internal system admin
- Estimated timeline for implementation
- Completed [vendor checklist on security and privacy](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=7423)
- Updated Privacy Documents from CISO (including updated PII data flow diagrams and DSAR compliance)
- Completed technical feasibility checklist from EAG (for architecture and infrastructure governance)
- Cost and Payment terms - How much, how often (annual, monthly, quarterly) and terms (Net 30, Net 60)
- Date to complete the purchase
- Contract start and end date
- Cancellation requirements
- Is this budgeted

This memo will be reviewed and approved by the following:

- CTO, EVP Finance and VP of IT Operations
- Security review by CISO
    - email to Suits@versprite.com with cc to Justin Fontenot <ravage@versprite.com> and Tony UV <mando@versprite.com>
- EVP Legal

In most cases, the approvers will require all sections listed above to be included in the memo to approve the purchase. At their discretion, the approvers can waive specific sections from being included in the memo.


For renewal purchases with Annual Contract Value changes greater than 5 percent where the total purchase is greater than $5,000 OR 

(i) Directly interacts with one of our customer-facing products regardless of price

(ii) Interface or store Personally Identifiable Information (PII).  This applies to both customer-facing and internal systems (such as backoffice systems)

(iii) Interface or store other red data (including customer/prospect lists, trade secrets, or company confidential material). This applies to both customer-facing and internal systems (such as backoffice systems)


A memo is required that outlines:

- Completed UPDATED vendor checklist on security and privacy
- Any key changes to Terms and Conditions

All purchases must be supported by a ReQlogic requisition before purchase even if payment method is a credit card. All purchases made before August 15, 2019 will not be subject to new purchase requirements, however renewal requirements would apply.
