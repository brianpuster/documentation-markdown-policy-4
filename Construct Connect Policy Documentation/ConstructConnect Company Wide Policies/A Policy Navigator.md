title: DELEGATION OF AUTHORITY - POLICY NAVIGATOR 
author: Dave Storer, Controller
published: 2020-01-13


Please see [Delegation of Authority Name Directory](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=7559) for authority levels by individual.


<div class="mermaid">
graph LR

A[Vendor Purchase] -->|small one off|T[Travel or Incidentals]
TP --> C[Use P Card or Reimburse] --> Concur
T --> TP[TravelPolicy]
A --> ROP[Lease or Charitable]
A --> FormerEmployee --> DOA
A --> CapEx --> DOA
ROP --> DOA[See ROPER DoA]
A --> S[Software or Consulting] --> SP[SoftwarePolicy]
S --> ConsultingPolicy --> R[ReqLogic]
SP -->R
DOA --> R
A --> AO[All Other] --> R
R --> REQ[Requistion]
REQ --> Approved
Approved --> Sign
Sign --> Buy

subgraph Type
ROP
S
AO
T
CapEx
FormerEmployee 
end

subgraph Governing Policy
ConsultingPolicy
SP
DOA
TP
end

click ConsultingPolicy "https://policydocs.buildone.co/docs/Construct%20Connect%20Policy%20Documentation/ConstructConnect%20Consulting%20and%20Professional%20Services%20Purchasing%20Policy"

click SP "https://policydocs.buildone.co/docs/Construct%20Connect%20Policy%20Documentation/ConstructConnect%20Customer%20Suspension,%20Cancellation%20and%20Write-off%20Policy"

click DOA "https://policydocs.buildone.co/docs/Construct%20Connect%20Policy%20Documentation/ConstructConnect%20Delegation%20of%20Authority"

click TP "https://policydocs.buildone.co/docs/Construct%20Connect%20Policy%20Documentation/ConstructConnect%20Travel%20and%20Entertainment%20Policy"

click R "https://reqlogic.buildone.co/reqlogic/Login.aspx"

click Concur "https://www.concursolutions.com/nui/signin/v2"

</div>

- For any questions, please contact <accountspayable@constructconnect.com>.
